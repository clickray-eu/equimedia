$(window).scroll(function(){
	scrollHeader();
})




function scrollHeader() {
   
   if (  $(".header--transparent").length == 1 ) {
   
   		var logoURL = '';
        
        if ($(document).scrollTop() > 20) {
            $(".header--transparent").addClass('scroll');
            logoURL = $('.header--transparent .widget-type-logo img').attr('src').split('white').join('black');
            $('.widget-type-logo img').attr('src', logoURL);

            
        } else {
            $(".header--transparent").removeClass('scroll');
             logoURL = $('.header--transparent .widget-type-logo img').attr('src').split('black').join('white');
            $('.widget-type-logo img').attr('src', logoURL);
           
        }
     }else return;
};
